<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Maplocation extends Administrator_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Company_map_model');
		$this->load->model('modules/Company_model');
	}
	
	/**
	 * public function index()
	 * Runs as default when this controller requested if any other method is not specified in route file.
	 * Collects all data (buttons, table columns, fields, pagination config, breadcrumb links) which will be displayed on index page of this controller (generally it contains rows of database result). At final sends data to target template.
	 */
	public function index()
	{
		$this->data['title'] = translate('index_title');
        $this->data['subtitle'] = translate('index_description');
        
        if($this->input->method() == 'post') { 
            $this->db->empty_table('company_map');
            if($this->input->post('companies')) {
                foreach($this->input->post('companies') as $company) {
                    $this->Company_map_model->insert(['company_id' => $company]);
                }
            }
        }

        $companiez = $this->Company_map_model->with_trashed()->all();

        $cmp_id = [];
        if($companiez) {
            foreach($companiez as $cmp) {
                $cmp_id[] = $cmp->company_id;
            }
        }

        $map_companies = $this->Company_model->filter(['id IN('.implode(',', $cmp_id).')' => NULL])->with_translation()->all();
		if($map_companies) {
			foreach($map_companies as $row)
			{	
				$data = new stdClass();
				$data->id 			= $row->id;
				$data->name 			= $row->name;
				$this->data['companies'][] = $data;
			 }
        }
        
        


		$this->template->render();
    }
    
    public function search() {

        $query = $this->input->get('query');

        $companies = $this->Company_model->filter(['name LIKE "'.$query.'%"' => NULL])->with_translation()->limit(10)->all();
        
        $this->data['companies'] = [];
        if($companies) {
            foreach($companies as $company) {
                $this->data['companies'][] = [
                    'id' => $company->id,
                    'name' => $company->name
                ];
            }
        }

        $this->template->json($this->data['companies']);
    }
}

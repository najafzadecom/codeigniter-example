<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maplocation_model extends Core_Model
{

    public $table = 'company_map';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
    }
}
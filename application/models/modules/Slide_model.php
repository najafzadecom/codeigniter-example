<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slide_model extends Core_Model
{

    public $table = 'slide';
    public $primary_key = 'id';
    public $protected = [];
    public $table_translation = 'slide_translation';
    public $table_translation_key = 'slide_id';
    public $table_language_key = 'language_id';

    public $timestamps = true;

    public function __construct()
    {
        parent::__construct();
    }
}